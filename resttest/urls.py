from django.conf.urls import patterns, url, include
from . import views
from django.contrib.auth import get_user_model
from django.contrib import admin


User = get_user_model()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.RootView.as_view(), name='root'),
    url(r'^register$', views.RegistrationView.as_view(), name='register'),
    url(r'^login$', views.LoginView.as_view(), name='login'),
    url(r'^password/forgot-password$', views.PasswordResetView.as_view(), name='forgot_password'),
    url(r'^docs/', include('rest_framework_swagger.urls')),
)